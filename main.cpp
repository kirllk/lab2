#include <iostream>
#include <memory>
#include <map>

class ElectronicJournal {
 protected:
  std::string _name;
  std::string _surname;
  std::string _lastname;
  std::string _kaf;
  std::string _academicDegree;
  int _yaer;
  int _rating;
 public:
  virtual void print(int id) = 0;
  ElectronicJournal(std::string_view name,
					std::string_view surname,
					std::string_view lastname,
					std::string_view kaf,
					std::string_view academicDegree,
					int year,
					int rating):
	  _name(name),
	  _surname(surname),
	  _lastname(lastname),
	  _kaf(kaf),
	  _academicDegree(academicDegree),
	  _yaer(year),
	  _rating(rating) { }
};

class Bachelor: public ElectronicJournal {
 public:
  void print(int id) override  {
	std::cout  << "Запись для баколавра" << std::endl
			   << "ID Студента: [" << id << "]" << std::endl
			   << "ФИО Студента: " << _lastname << _name << _surname << std::endl
			   << "Год поступления: " << _yaer << std::endl
			   << "Кафедра: " << _kaf << std::endl
			   << "Ученая степень: " << _academicDegree << std::endl
			   << "Рейтинг: " << _rating << std::endl << std::endl;

  };
  Bachelor(std::string_view name,
		   std::string_view surname,
		   std::string_view lastname,
		   std::string_view kaf,
		   std::string_view academicDegree,
		   int year,
		   int rating): ElectronicJournal(name, surname, lastname, kaf , academicDegree , year , rating) { }
};

class Master: public ElectronicJournal {
 public:
  void print(int id) override  {
	std::cout  << "Запись для магистров" << std::endl
	<< "ID Студента: [" << id << "]" << std::endl
	<< "ФИО Студента: " << _lastname << _name << _surname << std::endl
	<< "Год поступления: " << _yaer << std::endl
	<< "Кафедра: " << _kaf << std::endl
	<< "Ученая степень: " << _academicDegree << std::endl
	<< "Рейтинг: " << _rating << std::endl << std::endl;
  };
  Master(std::string_view name,
		 std::string_view surname,
		 std::string_view lastname,
		 std::string_view kaf,
		 std::string_view academicDegree,
		 int year,
		 int rating): ElectronicJournal(name, surname, lastname, kaf , academicDegree , year , rating) { }
};

class Graduate_student: public ElectronicJournal {
 public:
  void print(int id) override  {
	std::cout  << "Запись для аспиранта" << std::endl
			   << "ID Студента: [" << id << "]" << std::endl
			   << "ФИО Студента: " << _lastname << _name << _surname << std::endl
			   << "Год поступления: " << _yaer << std::endl
			   << "Кафедра: " << _kaf << std::endl
			   << "Ученая степень: " << _academicDegree << std::endl
			   << "Рейтинг: " << _rating << std::endl << std::endl;
  };
  Graduate_student(std::string_view name,
				   std::string_view surname,
				   std::string_view lastname,
				   std::string_view kaf,
				   std::string_view academicDegree,
				   int year,
				   int rating): ElectronicJournal(name, surname, lastname, kaf , academicDegree , year , rating) { }
};


class Factory {
 public:
  virtual std::shared_ptr<ElectronicJournal> produce(std::string _name, std::string _surname, std::string _lastname, std::string _kaf, int _yaer, int _rating) = 0;
};

class BachelorFactory: public Factory {
 public:
  std::shared_ptr<ElectronicJournal> produce(std::string _name, std::string _surname, std::string _lastname, std::string _kaf, int _yaer, int _rating) override {
	return std::make_shared<Bachelor>( _name, _surname, _lastname, _kaf , "Бакалавр" , _yaer , _rating);
  };
};

class MasterFactory: public Factory {
 public:
  std::shared_ptr<ElectronicJournal> produce(std::string _name, std::string _surname, std::string _lastname, std::string _kaf, int _yaer, int _rating) override {
	return std::make_shared<Master>( _name, _surname, _lastname, _kaf, "Магистр", _yaer , _rating);
  };
};

class Graduate_studentFactory: public Factory {
 public:
  std::shared_ptr<ElectronicJournal> produce(std::string _name, std::string _surname, std::string _lastname, std::string _kaf, int _yaer, int _rating) override {
	return std::make_shared<Graduate_student>( _name, _surname, _lastname, _kaf, "Аспирант",  _yaer , _rating);
  };
};

int main() {
  std::multimap<int, std::shared_ptr<ElectronicJournal>> col;
  BachelorFactory bachelorFactory;
  MasterFactory masterFactory;
  Graduate_studentFactory graduate_studentFactory;
  col.insert(std::move(std::make_pair(1234, bachelorFactory.produce("Кирилл" ,"Дмитриевич","Краснов","ИУ6",2018,30))));
  col.insert(std::move(std::make_pair(2123, masterFactory.produce("Петр" ,"Петрович","Иванов","СМ4",2015,21))));
  col.insert(std::move(std::make_pair(2344, graduate_studentFactory.produce("Иван" ,"Александрович","Горшков","ФН2",2012,9))));

  for (const auto& item: col) {
	item.second->print(item.first);
  }
  return 0;
}
